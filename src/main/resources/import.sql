INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(1,'Fermin','Flores','ferminfloresas@hotmail.com','2017-08-28', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(2,'Edgar','Loeffelman','eloeffelman@gmail.com','2021-05-11', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(3,'Junior','Flores','juniorflores@gmail.com','2021-04-17', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(4,'Lucero','Jasso','lucerojasso@gmail.com','2021-07-08', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(5,'Peluche','Flores','peluche@gmail.com','2021-01-16', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(6,'Filiberto','Jasso','filibertojasso@gmail.com','2021-05-19', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(7,'Ringo','Flores','ringoflores@gmail.com','2021-09-12', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(8,'Gerardo','Jasso','gerardojasso@gmail.com','2021-05-28', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(9,'Jakie','Flores','jakie@gmail.com','2021-07-25', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(10,'Sophia','Jasso','sophiajasso@gmail.com','2021-06-10', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(11,'Happy','Flores','happyflores@gmail.com','2021-07-20', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(12,'Coddy','Jasso','coddyjasso@gmail.com','2021-09-12', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(13,'Dolores','Jasso','doloresjasso@gmail.com','2021-12-12', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(14,'Tommy','Jasso','tommyjasso@gmail.com','2021-06-24', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(15,'Dante','Flores','danteflores@gmail.com','2021-07-15', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(16,'Fermin','Lopez','ferminlopez@gmail.com','2021-08-26', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(17,'leticia','Jasso','leticiajasso@gmail.com','2021-09-03', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(18,'Herbert','Loeffelman','herbertloeffelman@gmail.com','2021-5-16', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(19,'Theo','Loeffelman','theoloeffelman@gmail.com','2021-03-10', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(20,'Walter','Loeffelman','walterloeffelman@gmail.com','2021-04-27', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(21,'Xochicuauitl','Gleason','xochgleason@gmail.com','2021-01-25', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(22,'Ramon','Torres','ramontorres@gmail.com','2021-02-24', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(23,'Florentino','Zabala','florentinozabala@gmail.com','2021-03-23', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(24,'Dante','Sparda','dsparda@gmail.com','2021-06-22', '');
INSERT INTO clientes (id, nombre, apellido, email, create_at, foto) VALUES(25,'Nero','Sparda','nsparda@gmail.com','2021-08-21', '');

/* Tabla Productos*/
INSERT INTO productos (nombre, precio, create_at) VALUES('Panasonic Pantalla LCD', 254578, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Sony Camara Digital DSC-W320B', 223345, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Apple iPod shuffle', 657898, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Apple airPods', 3500, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Hisense Pantalla 4k', 5000, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('POCO Phone Ultra89', 567899, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Mouse RataMax200', 234243, NOW());

/* Creamos algunas Facturas */
INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura equipos de oficina', null, 1, NOW());
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES (1, 1, 1);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES (2, 1, 4);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES (1, 1, 5);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES (1, 1, 7);


INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura Bicicleta', 'Es una bicicleta muy importante', 1, NOW());
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES (3, 2, 6);
